<section class="row">
    <div class="row">
        <div class="col-lg-8">
            <div>
                <h3 aligne="" class="row">&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; We are always here to listne.Tell us
                    your story</h3>
                <br />
                @if($message = Session::get('success'))

                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                <div class="container py-5">
                    <div class="row">
                        <div class="col-md-10 mx-auto">
                            <div class="col-sm-6">
                                <form>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <label for="inputFirstname">Name</label>
                                            <input type="text" class="form-control" id="inputName" placeholder="Name">
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="inputEmailaddress">Email Address</label>
                                            <input type="text" class="form-control" id="inputEmailAddress"
                                                placeholder="Email Address">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <label for="inputPhone">Phone</label>
                                            <input type="text" class="form-control" id="inputPhone"
                                                placeholder="Contact Number">
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="inputCompany">Company</label>
                                            <input type="text" class="form-control" id="inputCompany"
                                                placeholder="Company Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <label for="inputCountry">Country</label>
                                            <input type="text" class="form-control" id="inputCountry"
                                                placeholder="Country">
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="inputZipCode">Zip Code</label>
                                            <input type="text" class="form-control" id="inputZipCode"
                                                placeholder="Zip Codd">
                                        </div>

                                        <div class="col-md-12">
                                            <label for="message">Message</label>
                                            <textarea name="message" class="form-control" id="message"
                                                placeholder="Enter Message Detail"></textarea>

                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-primary px-4 float-right">Save</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>







        </div>


        <div class="col-lg-4">


            <div class="row">
                <div class="col-sm-12">

                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-md-12 info">
                                <i class="ion-ios-location-outline"></i>
                                <p>10, Mc Donald Avenue, Sunset Park, Newyork</p>
                            </div><br>

                            <div class="col-md-12 info">
                                <i class="ion-ios-email-outline"></i>
                                <p>info@example.com</p>
                            </div><br>
                            <div class="col-md-12 info">
                                <i class="ion-ios-telephone-outline"></i>
                                <p>+99 999 9999</p>
                            </div>
                        </div>

                        <div id="contact">
                            <div class="container-fluid">

                                <div class="section-header">
                                    <h3>Contact Us</h3>
                                </div>

                                <div class="row wow fadeInUp">

                                    <div class="col-lg-12">
                                        <div class="map mb-12 mb-lg-0">
                                            <iframe
                                                src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621"
                                                frameborder="0" style="border:0; width: 100%; height: 50%;"
                                                allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div><!-- #contact -->

            </main>
        </div>
    </div>
    </div>
    <script>
    </div>

    </div>

    </section>


    $(document).ready(function() {

                if ($("#contact_form").length > 0) {
                    $('#contact_form').validate({
                            rules: {
                                name: {
                                    required: true,
                                    maxlength: 50
                                },
                                phone: {
                                    required: true,
                                    maxlength: 50
                                },
                                country: {
                                    required: true,

                                },
                                email: {
                                    required: true,
                                    maxlength: 50,
                                    email: true
                                },
                                company: {
                                    required: true,

                                },
                                zip_code: {
                                    required: true,
                                    maxlength: 50
                                },
                                message: {
                                    required: true,
                                    minlength: 50,
                                    maxlength: 500
                                }
                            },
                            messages: {
                                name: {
                                    required: 'Enter Name Detail',
                                    maxlength: 'Name should not be more than 50 character'
                                },
                                messages: {
                                    phone: {
                                        required: 'Enter Phone Detail',
                                        maxlength: 'phone should not be more than 50 character'
                                    },
                                    message: {
                                        required: 'Enter Country Detail',
                                        country: 'Enter Valid country Detail',
                                    },
                                    messages: {
                                        company: {
                                            required: 'Enter Company Detail',

                                        },

                                        message: {
                                            required: 'Enter Message Detail',
                                            minlength: 'Message Details must be minimum 50 character long',
                                            maxlength: 'Message Details must be maximum 500 character long',
                                        },
                                    }
                                });
                        }

                    });
    </script>