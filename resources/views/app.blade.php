<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from winsoft.netlify.app/index-8.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 06 Jan 2021 16:43:07 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="utf-8">
    <!-- Responsive Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- IE Compatibility Meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Page Title -->
    <title>Winsoft Landing Page</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Bootstrap Stylesheet -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700" rel="stylesheet">

    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/colors/color-6.css">
    @yield('head_styles')

    <!--[if lt IE 9]>
         <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
         <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
</head>

<body>

    <!-- Page Holder -->
    <div class="page-holder">

        <!-- ======================================= 
        ==Start  Nav section==  
        =======================================-->
        @include('includes.nav')
        <!-- ======================================= 
        ==End  Nav section==  
        =======================================-->

        <div>
            @yield('content')
        </div>

        <!-- ======================================= 
        ==Start footer section==  
        =======================================-->
        @include('includes.footer')
        <!-- ======================================= 
        ==End footer section==  
        =======================================-->

        <!-- ======================================= 
        ==Start preloader section==  
        =======================================-->
        <div class="preloader">
            <div class="loader">
                <div class="loader-inner"></div>
            </div>
        </div>
        <!-- ======================================= 
        ==End preloader section==  
        =======================================-->

    </div>
    <!-- End Page Holder -->


    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/script.js"></script>
</body>

<!-- Mirrored from winsoft.netlify.app/index-8.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 06 Jan 2021 16:43:31 GMT -->

</html>