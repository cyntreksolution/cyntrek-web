@extends('app')
@section('content')

<div id="home" class="header-3 header-bg-3 new-demo">
    <div class="container">
        <div class="header-3-content row d-flex align-items-center"><div id="home"></div>

            <div class="col-md-5 col-sm-12">
                <h1 class="has-line mb-20 w-text">We promise to bring <span class="bold yellow">the best
                        solutions</span> for your business</h1>
                <p class="lead g-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe aspernatur
                    adipisci minus assumenda omnis, possimus beatae aliquam explicabo, recusandae veritatis earum
                    repellendus, impedit aliquid deleniti. </p>
                
            </div>

            <div class="col-md-7 hidden-sm hidden-xs">
                <img src="images/img-home-hero.svg" class="center-block mt-70 mb-50 width-120" alt="image">
            </div>

        </div>
    </div>
</div>

<!-- ======================================= 
        ==Start services-1 section==  
        =======================================-->
<section id="services" class="special-feat">
    <div class="container">

        <div class="col-md-4 col-sm-12 col-xs-12 text-left">
            <div><a href="#" class="orange">learn more</a></div>
            <h2 class="has-line dark-text"> <span class="bold">Manage conversastions</span></h2>
            <p class="lead mb-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
            <a href="#" class="more-btn gradient-btn mt-10"> read more</a>
        </div>


        <!--Service Box-->
        <div class="service-box-one mt-s col-md-4 col-sm-6 col-xs-12">
            <div class="inner-box text-left">
                <div class="icon-box"><img src="images/elements/icon1.png" alt=""></div>
                <h3><a href="#">TECHNICAL SUPPORT</a></h3>
                <div class="text">Capitalise on low hanging fruit to identify a ballpark value added activity to beta
                    test Capitalise on low hanging fruit to identify a ballpark value added activity to beta test.</div>
            </div>
        </div>

        <!--Service Box-->
        <div class="service-box-one mt-s col-md-4 col-sm-6 col-xs-12">
            <div class="inner-box text-left">
                <div class="icon-box"><img src="images/elements/icon2.png" alt=""></div>
                <h3><a href="#">INVESTMENT ADVISOR</a></h3>
                <div class="text">Capitalise on low hanging fruit to identify a ballpark value added activity to beta
                    test Capitalise on low hanging fruit to identify a ballpark value added activity to beta test.</div>
            </div>
        </div>

        <!--Service Box-->
        <div class="service-box-one mt-s col-md-4 col-sm-6 col-xs-12">
            <div class="inner-box text-left">
                <div class="icon-box"><img src="images/elements/icon3.png" alt=""></div>
                <h3><a href="#">MARKETING BASE</a></h3>
                <div class="text">Capitalise on low hanging fruit to identify a ballpark value added activity to beta
                    test Capitalise on low hanging fruit to identify a ballpark value added activity to beta test.</div>
            </div>
        </div>

        <!--Service Box-->
        <div class="service-box-one mt-s col-md-4 col-sm-6 col-xs-12">
            <div class="inner-box text-left">
                <div class="icon-box"><img src="images/elements/icon4.png" alt=""></div>
                <h3><a href="#">WEALTH MANAGEMENT</a></h3>
                <div class="text">Capitalise on low hanging fruit to identify a ballpark value added activity to beta
                    test Capitalise on low hanging fruit to identify a ballpark value added activity to beta test.</div>
            </div>
        </div>

    </div>
</section>
<!-- ======================================= 
        ==End services-1 section==  
        =======================================-->


<!-- ======================================= 
        ==Start services-1 section==  
        =======================================-->
<section id="features" class="section-padding-0-70 no-pt-sm">
    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col-md-6 col-sm-12 col-xs-12 text-left">
                <div><a href="#" class="orange">learn more</a></div>
                <h2 class="has-line dark-text"> <span class="bold">comprehensive Features</span></h2>
                <p class="lead mb-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>

                <ul class="s-list list-unstyled mb-20">
                    <li><span class="fa fa-check"></span>Donec facilisis velit eu est phasellus consequat quis nostrud
                    </li>
                    <li><span class="fa fa-check"></span>Aenean vitae quam. Vivamus et nunc nunc conseq</li>
                    <li><span class="fa fa-check"></span>Sem vel metus imperdiet lacinia enean </li>
                    <li><span class="fa fa-check"></span>Dapibus aliquam augue fusce eleifend quisque tels</li>
                    <li><span class="fa fa-check"></span>Dapibus aliquam augue fusce tels</li>
                </ul>

                <a href="#" class="more-btn gradient-btn mt-20"> read more</a>
            </div>


            <!--Service Box-->
            <div class="col-md-6 col-sm-12 col-xs-12 mt-s">

                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="service-box-one bg-1 new-feat">
                            <div class="inner-box">
                                <img src="images/icons/f1.png" alt="" class="block-icon">
                                <h3 class="w-text">Email Marketing</h3>
                                <div class="g-text text">Capitalise on low hanging fruit to identify a balpark value add
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 ">
                        <div class="service-box-one bg-2 new-feat mt-lg-30 mb-lg-0">
                            <div class="inner-box">
                                <img src="images/icons/f3.png" alt="" class="block-icon">
                                <h3 class="w-text">Content Marketing</h3>
                                <div class="g-text text">Capitalise on low hanging fruit to identify a balpark value add
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="service-box-one bg-3 new-feat">
                            <div class="inner-box">
                                <img src="images/icons/f2.png" alt="" class="block-icon">
                                <h3 class="w-text">Paid Advertising</h3>
                                <div class="g-text text">Capitalise on low hanging fruit to identify a balpark value add
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 mt-lg-30">
                        <div class="service-box-one bg-4 new-feat">
                            <div class="inner-box">
                                <img src="images/icons/f4.png" alt="" class="block-icon">
                                <h3 class="w-text">Search Optimization</h3>
                                <div class="g-text text">Capitalise on low hanging fruit to identify a balpark value add
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- ======================================= 
        ==End services-1 section==  
        =======================================-->

<!-- ======================================= 
        ==Start statistics section==  
        =======================================-->
<section id="about" class="no-padding-bottom no-padding-top">
    <div class="container">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="inner-box text-left mt-s">
                <img src="images/about3.png" class="img-responsive center-block" alt="">
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12 text-left mt-s">
            <div><a href="#" class="orange">learn more</a></div>
            <h2 class="has-line dark-text"> <span class="bold">Take your business directly to the next level</span></h2>
            <p class="lead mb-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus beatae, sed ab.
                Expedita qui deleniti laborum facilis, cupiditate adipisci ea, eos id quaerat quis. Iusto suscipit,
                dolore veniam earum doloremque!</p>

            <a href="#" class="more-btn gradient-btn mt-20"> read more</a>
        </div>

    </div>
</section>
<!-- ======================================= 
        ==End statistics section==  
        =======================================-->

<section class="download padding-top-small padding-bottom-small clearfix">
    <span class="text-bg">Try our template</span>

    <div class="container">
        <div class="row d-flex align-items-center">

            <div class="col-12 col-md-7 offset-lg-0 col-sm-12">
                <div class="who-we-contant relative extended v2">

                    <div><a href="#">Get The Software</a></div>
                    <h2 class="has-line"> <span class="bold">Try Our Software</span></h2>
                    <p class="lead g-text mb-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga soluta
                        aliquam, est voluptate cupiditate repellat dolor id explicabo harum. Beatae libero mollitia
                        suscipit unde.</p>
                    <div class="clearfix"></div>
                    <div class="header-button mt-20">
                        <a href="#" class="download-btn"><i class="fa fa-android"></i> Google Store</a>
                        <a href="#" class="download-btn"><i class="fa fa-apple"></i> Apple Store</a>
                    </div>

                </div>
            </div>
            <div class="col-12 col-md-5 offset-lg-0 col-sm-12 mt-s">
                <div class="welcome-meter fadeInUp" data-wow-delay="0.7s">
                    <img src="images/dashboard1.png" class="floating img-responsive center-block" alt="">
                </div>
            </div>

        </div>
    </div>
</section>

<!-- ======================================= 
        ==Start testimonials section==  
        =======================================-->
<section id="test" class="testimonial section-padding-100-70"
    style="background-image: url(images/header-images/bg/test-bg.png)">
    <div class="container text-center">
        <div class="row d-flex align-items-center">
            <div class="col-md-4 col-sm-12 col-xs-12 text-left">
                <div><a href="#" class="orange"> App Reviews</a></div>
                <h2 class="has-line dark-text"> <span class="bold">What our Clients are Saying</span></h2>
                <p class="lead mb-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus beatae, sed ab.
                    Expedita qui deleniti laborum facilis, cupiditate adipisci ea, eos id quaerat quis. Iusto suscipit,
                    dolore veniam earum doloremque!</p>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 testimonials-style-two">
                <!--testimonial Item-->
                <div class="item ">
                    <div class="testimonial-item has-shadow">
                        <div class="inner-box">
                            <figure class="author-thumb img-circle"><img class="img-circle"
                                    src="images/testimonials/author-thumb-1.jpg" alt="image"></figure>
                            <div class="text">Leverage agile frameworks to provide a robust synopsis for high level
                                overviews. Iterative strategy collaborative foster further the overall value
                                proposition. </div>
                            <div class="info">Thomas Barkley - <span class="designation">Founder</span></div>
                            <div class="quote-icon"><span class="fa fa-quote-left"></span></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 testimonials-style-two">
                <!--testimonial Item-->
                <div class="item ">
                    <div class="testimonial-item has-shadow">
                        <div class="inner-box">
                            <figure class="author-thumb img-circle"><img class="img-circle"
                                    src="images/testimonials/author-thumb-2.jpg" alt="image"></figure>
                            <div class="text">Leverage agile frameworks to provide a robust synopsis for high level
                                overviews. Iterative strategy collaborative foster further the overall value
                                proposition. </div>
                            <div class="info">Thomas Barkley - <span class="designation">Founder</span></div>
                            <div class="quote-icon"><span class="fa fa-quote-left"></span></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- ======================================= 
        ==Start testimonials section==  
        =======================================-->


<!-- ======================================= 
        ==Start subscribe section==  
        =======================================-->
<section id="sub" class="subscribe padding-top-small padding-bottom-small">
    <div class="container text-center">
        <div class="subscribe-newsletter">
            <h2>Subscribe To Our Newsletter</h2>
            <p class="lead text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent feugiat ipsum
                maximus malesuada. Donec non ex faucibus, fringilla tellus eget, maximus arcu. Duis et eros nisl.</p>
            <form>
                <input type="text" placeholder="type your email">
                <label><input type="submit" class="more-btn" value="Subscribe Now"></label>
            </form>
        </div>
    </div>
</section>
<!-- ======================================= 
        ==End subscribe section==  
        =======================================-->
@endsection