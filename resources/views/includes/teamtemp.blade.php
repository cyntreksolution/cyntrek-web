<div class="header-3 header-bg-3 new-demo"> 
<style type="text/css">
    /*OurTeam Start*/
.team{
    padding: 100px 0px;
   
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    background-attachment: fixed;
}
.team_top h1{
    text-transform: uppercase;
    color: ##000000;
    font-size: 60px;
    font-weight: 700;
}    
    .team_top h2{
    text-transform: uppercase;
    color: ##000000;
    font-weight: 700;
}
.team_top p{
    font-size: 18px;
    color: #ffffff;
    font-weight: 500;
    max-width: 750px;
    margin: 0 auto;
}
.member{
    position: relative;
    margin-top: 70px;
    margin-bottom: 70px;
    background-color: #fff;
    padding: 20px;
}
.img img{
    width: 130px;
    border-radius: 50%;
}
.m_text h3{
    font-weight: 800;
    font-size: 22px;
    margin-top: 20px;
    color: #00cdcd;
}
.m_text h5{
    font-size: 18px;
    margin-bottom: 20px;
}
.m_text p{
    font-weight: 400;
    line-height: 30px;
    letter-spacing: 1px;
    margin-bottom: 0px;
}
.m_menu{
    margin: 0;
    padding: 0;
    position: absolute; 
    left: 0;
    right: 0;
    bottom: -35px;
    background: #fff;
    padding: 10px;
    opacity: 0;
}
.m_menu ul{
    margin-bottom: 0px;
}
.m_menu ul li a i{
    color: #F2CE5F;
}
.m_menu ul li a{
    display: block;
    padding: 7px;
    font-size: 18px;
}
.member:hover .m_menu{
    opacity: 1;
    bottom: -40px;
    transition: all 0.5s ease-in-out;
    -webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
}
.member:hover .m_text h3{
    color: #F2CE5F;
    transition: all 0.5s ease-in-out;
    -webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
}
/*OurTeam end*/
</style>


    <!-- OurTeam Start -->
    <section class="team">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="team_top text-center">
                        <h1 class="mb-2">Our Team</h1>
                        <p>Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore lorem ipsum amet madolor sit amet.</p></br></br>
                        <h2 class="mb-2">Our Strengths</h2>
                        <p>Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore lorem ipsum amet madolor sit amet.</p></br></br>
                        <h2 class="mb-2">Our Weaknesses</h2>
                        <p>Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore lorem ipsum amet madolor sit amet.</p></br></br>
                        
                    </div>
                </div>
            </div>
            <!-- 2nd row -->
            <div class="row">
                <!-- single member -->
                <div class="col-lg-3 col-md-6">
                    <div class="member text-center">
                        <div class="img">
                            <img src="https://i.postimg.cc/YCpBf6Gg/nabed.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="m_text">
                            <h3>Nabed Khan</h3>
                            <h5>Front-End Developer</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio provident dolores.</p>
                        </div>
                        <div class="m_menu">
                            <ul class="list-inline">
                              <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                              <li class="list-inline-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
                              <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                              <li class="list-inline-item"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- single member -->
                <div class="col-lg-3 col-md-6">
                    <div class="member text-center">
                        <div class="img">
                            <img src="https://i.postimg.cc/YC3kJnKb/team-1.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="m_text">
                            <h3>Emilia Noah</h3>
                            <h5>Creative Designer</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio provident dolores.</p>
                        </div>
                        <div class="m_menu">
                            <ul class="list-inline">
                              <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                              <li class="list-inline-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
                              <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                              <li class="list-inline-item"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- single member -->
                <div class="col-lg-3 col-md-6">
                    <div class="member text-center">
                        <div class="img">
                            <img src="https://i.postimg.cc/CLbw4Mvy/team-2.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="m_text">
                            <h3>Emilia Noah</h3>
                            <h5>Digital Marketer</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio provident dolores.</p>
                        </div>
                        <div class="m_menu">
                            <ul class="list-inline">
                              <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                              <li class="list-inline-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
                              <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                              <li class="list-inline-item"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- single member -->
                <div class="col-lg-3 col-md-6">
                    <div class="member text-center">
                        <div class="img">
                            <img src="https://i.postimg.cc/jSKt3V9D/team-3.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="m_text">
                            <h3>Emilia Noah</h3>
                            <h5>Back-End Developer</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio provident dolores.</p>
                        </div>
                        <div class="m_menu">
                            <ul class="list-inline">
                              <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                              <li class="list-inline-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
                              <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                              <li class="list-inline-item"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <!-- OurTeam End -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    