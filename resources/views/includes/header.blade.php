  <!-- ======================================= 
        ==Start header-3 section==  
        =======================================-->
        <div id="home" class="header-3 header-bg-3 new-demo">
          <div class="container">
            <div class="header-3-content row d-flex align-items-center">

              <div class="col-md-5 col-sm-12">
                <h1 class="has-line mb-20 w-text">We promise to bring <span class="bold yellow">the best solutions</span> for your business</h1>
                <p class="lead g-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe aspernatur adipisci minus assumenda omnis, possimus beatae aliquam explicabo, recusandae veritatis earum repellendus, impedit aliquid deleniti. </p> 
              </div>

              <div class="col-md-7 hidden-sm hidden-xs">
                <img src="images/img-home-hero.svg" class="center-block mt-70 mb-50 width-120" alt="image">
              </div> 

            </div>
          </div>
        </div>
        <!-- ======================================= 
        ==End header-3 section==  
        =======================================-->