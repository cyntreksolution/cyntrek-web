const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);


mix.copy('resources/css', 'public/css');

mix.copy('resources/js/bootstrap copy.js', 'public/js');
mix.copy('resources/js/jquery.counterup.min.js', 'public/js');
mix.copy('resources/js/jquery.easing.min.js', 'public/js');
mix.copy('resources/js/jquery.min.js', 'public/js');
mix.copy('resources/js/owl.carousel.min.js', 'public/js');
mix.copy('resources/js/script.js', 'public/js');
mix.copy('resources/js/waypoints.min.js', 'public/js');
mix.copy('resources/images', 'public/images');
mix.copy('resources/fonts', 'public/fonts');