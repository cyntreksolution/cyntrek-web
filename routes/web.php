<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/contact', function () {
    return view('Contactus');
});
Route::get('/services', function () {
    return view('Services');
});


Route::get('/team', function () {
    return view('team');
});




Route::get('contact-us', 'ContactusController@index');

Route::post('contact-us/submit', 'ContactusController@submit');
